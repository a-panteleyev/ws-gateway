package main

import (
	"flag"
	"encoding/base64"
	"encoding/binary"
	"fmt"
	"log"
	"net/http"
	"net/url"
	"strconv"
	"time"
	"sync/atomic"

	"github.com/gorilla/websocket"
	"github.com/google/uuid"
)

type WSCmd struct {
	Archive   string `json:"archive,omitempty"`
	Method    string `json:"method"`
	Endpoint  string `json:"endpoint"`
	BeginTime string `json:"beginTime,omitempty"`
	Format    string `json:"format"`
	Keyframes bool	 `json:"keyFrames"`
	StreamId  string `json:"streamId"`
	Speed     int64  `json:"speed,omitempty"`
	Width     int64  `json:"width,omitempty"`
	Height    int64  `json:"height,omitempty"`
	Q         int64  `json:"q,omitempty"`
}
type WSPkt struct {
	StreamId string
	Timestamp uint64
	Payload	[]byte
}
type DoneSignal struct{}
type ByteChan chan []byte
type StreamChan struct {
	id string
	c ByteChan
}

const (
	M_PLAY          = "play"
	M_STOP          = "stop"
	MBYTE		= 1 << 20
)

var (
	backend  string
	auth  string
	port     int
	wsconn websocket.Conn
	tx = 0
	rx = 0
)

func counter() {
	for {
		time.Sleep(10 * time.Second)
		log.Printf(
			"tx %0.2fMB/s rx %0.2fMB/s\n",
			float64(tx) / MBYTE / 10,
			float64(rx) / MBYTE / 10,
		)
		tx = 0
		rx = 0
	}
}

func getContentType(format string) string {
	switch format {
	case "mp4":
		return "video/mp4"
	case "mjpeg":
		return "image/jpeg"
	default:
		return ""
	}
}
func parseMessage(data []byte) WSPkt {
	pos := 0
	idLen := int(binary.BigEndian.Uint16(data[pos:pos+2]));
	pos += 2
	streamId := string(data[pos:idLen+pos])
	pos += idLen
	ts := binary.BigEndian.Uint64(data[pos:pos+8])
	pos += 8
	// preroll := data[pos] == 0xFF
	// pos += 1
	payload := data[pos:]

	return WSPkt{
		StreamId: streamId,
		Timestamp: ts,
		Payload: payload,
	}
}
func sendCommands(wsc *websocket.Conn, sendCmd <-chan WSCmd, done<-chan DoneSignal) {
	for {
		select {
		case <-done:
			log.Println("wsc: sender stopped")
			return
		case cmd := <-sendCmd:
			log.Println("wsc->", cmd)
			if err := wsc.WriteJSON(&cmd); err != nil {
				log.Println("wsc: ", err)
				continue
			}
		}
	}
}
func distributePkts(
	wsc *websocket.Conn,
	count *uint64,
	receiver chan<-*WSPkt,
	done <-chan DoneSignal,
) {
	for {
		select {
		case <-done:
			log.Println("wsc: receiver stopped")
			return
		default:
			_, data, err := wsc.ReadMessage()
			if err != nil {
				log.Println("wsc: ", err)
				return
			}
			rx += len(data)
			pkt := parseMessage(data)
			for i := uint64(0); i <= atomic.LoadUint64(count); i++ {
				receiver<-&pkt
			}
		}
	}
}

func wsGateway(sendCmd chan<-WSCmd, count *uint64, receiver <-chan *WSPkt) http.HandlerFunc {
	return func (w http.ResponseWriter, r *http.Request) {
		log.Println(r.URL.RawQuery)
		opts, err := url.ParseQuery(r.URL.RawQuery)
		if err != nil {
			log.Panicln(err)
			return
		}
		var wscmd WSCmd

		uuid, _ := uuid.NewRandom()
		streamId := uuid.String()
		wscmd.StreamId = streamId
		wscmd.Method = M_PLAY
		wscmd.Speed = 1
		wscmd.Endpoint = opts.Get("endpoint")
		wscmd.Format = opts.Get("format")

		if (opts.Get("keyframes") != "") {
			wscmd.Keyframes = true
		} else {
			wscmd.Keyframes = false
		}
		if wscmd.Format == "mjpeg" {
			wscmd.Width, _ = strconv.ParseInt(opts.Get("width"), 10, 16)
			wscmd.Height, _ = strconv.ParseInt(opts.Get("height"), 10, 16)
			wscmd.Q, _ = strconv.ParseInt(opts.Get("q"), 10, 8)
		}
		if speed := opts.Get("speed"); speed != "" {
			wscmd.Speed, _ = strconv.ParseInt(speed, 10, 8)
		}
		if archive := opts.Get("archive"); archive != "" {
			wscmd.Archive = archive
		}
		if beginTime := opts.Get("begintime"); beginTime != "" {
			wscmd.BeginTime = beginTime
		}

		sendCmd<-wscmd
		w.Header().Set("Content-Type", getContentType(wscmd.Format))
		atomic.AddUint64(count, 1)
		for {
			pkt := <-receiver
			if pkt.StreamId != streamId {
				continue
			}
			_, err := w.Write(pkt.Payload)
			if err != nil {
				atomic.AddUint64(count, ^uint64(0))
				sendCmd<-WSCmd{
					StreamId: streamId,
					Method: M_STOP,
				}
				break
			}
			tx += len(pkt.Payload)
		}
	}
}

func main() {
	flag.StringVar(&backend, "srv", "ws://localhost:8000/ws", "Backend URI")
	flag.StringVar(&auth, "auth", "root:root", "<user>:<pass>")
	flag.IntVar(&port, "listen", 9999, "Listening port")
	flag.Parse()

	base64Auth := base64.StdEncoding.EncodeToString([]byte(auth))
	hdr := http.Header{ }
	hdr.Set("Authorization", fmt.Sprintf("Basic %s", base64Auth))

	wsconn, res, err := websocket.DefaultDialer.Dial(backend, hdr)
	if err != nil {
		log.Fatalln("Can't connect to", backend);
	}
	log.Println("Connected to", backend)
	log.Println("http", res.Status)


	sendCmd := make(chan WSCmd)
	receiver := make(chan *WSPkt)
	done := make(chan DoneSignal, 2)
	var stream_count uint64 = uint64(0)

	go counter()
	go distributePkts(wsconn, &stream_count, receiver, done)
	go sendCommands(wsconn, sendCmd, done)

	mux := http.NewServeMux()
	mux.HandleFunc("/", wsGateway(sendCmd, &stream_count, receiver))
	log.Println("Listening on", port)
	log.Fatal(http.ListenAndServe(fmt.Sprintf(":%d", port), mux))

	done<-DoneSignal{}
	done<-DoneSignal{}
	close(sendCmd)
	close(done)
	wsconn.Close()
}
