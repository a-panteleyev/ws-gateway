# Start
```
./ws-gateway
```
## Options

```
./ws-gateway --help
Usage of ./ws-gateway:
  -Basic Auth string
    	<user>:<pass> (default "root:root")
  -WebSocket backend string
    	Backend URI (default "ws://localhost:8000/ws")
  -listen int
    	Listening port (default 9999)
```

# Make requests

```
ffplay 'http://localhost:9999/?format=mp4&endpoint=NGP/DeviceIpint.2/SourceEndpoint.video:0:0'
```

## Query
### Required
- endpoint (_HOSTNAME/DeviceType/SourceEndpoint_)
- format (mp4/mjpeg)
### Additional
- begintime
- archive
- speed
- keyframes
### for MJPEG
- width
- height
- q (0..6)

